module github.com/pansicheng/pq

go 1.15

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/google/uuid v1.1.2
	github.com/pkg/errors v0.9.1
	github.com/transaction-wg/seata-golang v1.0.1
)

replace github.com/transaction-wg/seata-golang => github.com/transaction-wg/seata-golang v1.0.0-rc2
