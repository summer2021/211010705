DROP
database if EXISTS seata_order;
CREATE
database seata_order;
\c seata_order;

DROP TABLE if EXISTS branch_transaction;
CREATE SEQUENCE branch_transaction_seq;
CREATE TABLE branch_transaction
(
    sysno        bigint       NOT NULL DEFAULT NEXTVAL('branch_transaction_seq'),
    xid          varchar(128) NOT NULL,
    branch_id    bigint       NOT NULL,
    args_json    varchar(512)          DEFAULT NULL,
    state        smallint              DEFAULT NULL,
    gmt_create   timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    gmt_modified timestamp(0) NULL DEFAULT NULL,
    PRIMARY KEY (sysno),
    UNIQUE (xid),
    UNIQUE (xid, branch_id)
);

DROP TABLE if EXISTS so_item;
CREATE SEQUENCE so_item_seq;
CREATE TABLE so_item
(
    sysno          bigint NOT NULL DEFAULT NEXTVAL('so_item_seq'),
    so_sysno       bigint          DEFAULT NULL,
    product_sysno  bigint          DEFAULT NULL,
    product_name   varchar(64)     DEFAULT NULL,
    cost_price     decimal(16, 6)  DEFAULT NULL,
    original_price decimal(16, 6)  DEFAULT NULL,
    deal_price     decimal(16, 6)  DEFAULT NULL,
    quantity       int             DEFAULT NULL,
    PRIMARY KEY (sysno)
);
ALTER SEQUENCE so_item_seq RESTART WITH 1269646673999564801;

DROP TABLE IF EXISTS so_master;
CREATE TABLE so_master
(
    sysno                  bigint NOT NULL,
    so_id                  varchar(20)    DEFAULT NULL,
    buyer_user_sysno       bigint         DEFAULT NULL,
    seller_company_code    varchar(20)    DEFAULT NULL,
    receive_division_sysno bigint NOT NULL,
    receive_address        varchar(200)   DEFAULT NULL,
    receive_zip            varchar(20)    DEFAULT NULL,
    receive_contact        varchar(20)    DEFAULT NULL,
    receive_contact_phone  varchar(100)   DEFAULT NULL,
    stock_sysno            bigint         DEFAULT NULL,
    payment_type           smallint       DEFAULT NULL,
    so_amt                 decimal(16, 6) DEFAULT NULL,
    status                 smallint       DEFAULT NULL,
    order_date             timestamp(0) NULL DEFAULT NULL,
    payment_date           timestamp(0) NULL DEFAULT NULL,
    delivery_date          timestamp(0) NULL DEFAULT NULL,
    receive_date           timestamp(0) NULL DEFAULT NULL,
    appid                  varchar(64)    DEFAULT NULL,
    memo                   varchar(255)   DEFAULT NULL,
    create_user            varchar(255)   DEFAULT NULL,
    gmt_create             timestamp(0) NULL DEFAULT NULL,
    modify_user            varchar(255)   DEFAULT NULL,
    gmt_modified           timestamp(0) NULL DEFAULT NULL,
    PRIMARY KEY (sysno)
);

DROP TABLE IF EXISTS undo_log;
CREATE SEQUENCE undo_log_seq;

CREATE TABLE undo_log
(
    id            bigint       NOT NULL DEFAULT NEXTVAL('undo_log_seq'),
    branch_id     bigint       NOT NULL,
    xid           varchar(100) NOT NULL,
    context       varchar(128) NOT NULL,
    rollback_info text         NOT NULL,
    log_status    int          NOT NULL,
    log_created   timestamp(0) NOT NULL,
    log_modified  timestamp(0) NOT NULL,
    ext           varchar(100)          DEFAULT NULL,
    PRIMARY KEY (id)
);

ALTER SEQUENCE undo_log_seq RESTART WITH 27;

CREATE
INDEX idx_unionkey ON undo_log (xid,branch_id);
