DROP
database if EXISTS seata_product;
CREATE
database seata_product;
\c seata_product;

DROP TABLE IF EXISTS branch_transaction;
CREATE SEQUENCE branch_transaction_seq;
CREATE TABLE branch_transaction
(
    sysno        bigint       NOT NULL DEFAULT NEXTVAL('branch_transaction_seq'),
    xid          varchar(128) NOT NULL,
    branch_id    bigint       NOT NULL,
    args_json    varchar(512)          DEFAULT NULL,
    state        smallint              DEFAULT NULL,
    gmt_create   timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    gmt_modified timestamp(0) NULL DEFAULT NULL,
    PRIMARY KEY (sysno),
    UNIQUE (xid),
    UNIQUE (xid, branch_id)
);

DROP TABLE IF EXISTS inventory;
CREATE SEQUENCE inventory_seq;
CREATE TABLE inventory
(
    sysno             bigint check (sysno > 0)         NOT NULL DEFAULT NEXTVAL('inventory_seq'),
    product_sysno     bigint check (product_sysno > 0) NOT NULL,
    account_qty       int                                       DEFAULT NULL,
    available_qty     int                                       DEFAULT NULL,
    allocated_qty     int                                       DEFAULT NULL,
    adjust_locked_qty int                                       DEFAULT NULL,
    PRIMARY KEY (sysno)
);

ALTER SEQUENCE inventory_seq RESTART WITH 2;

BEGIN;
INSERT INTO inventory
VALUES (1, 1, 1000000, 1000000, 0, 0);
COMMIT;

DROP TABLE IF EXISTS product;
CREATE SEQUENCE product_seq;

CREATE TABLE product
(
    sysno              bigint check (sysno > 0) NOT NULL DEFAULT NEXTVAL('product_seq'),
    product_name       varchar(32)              NOT NULL,
    product_title      varchar(32)              NOT NULL,
    product_desc       varchar(2000)            NOT NULL,
    product_desc_long  text                     NOT NULL,
    default_image_src  varchar(200)                      DEFAULT NULL,
    c3_sysno           bigint                            DEFAULT NULL,
    barcode            varchar(30)                       DEFAULT NULL,
    length             int                               DEFAULT NULL,
    width              int                               DEFAULT NULL,
    height             int                               DEFAULT NULL,
    weight             double precision                  DEFAULT NULL,
    merchant_sysno     bigint                            DEFAULT NULL,
    merchant_productid varchar(20)                       DEFAULT NULL,
    status             smallint                 NOT NULL DEFAULT '1',
    gmt_create         timestamp(0)             NOT NULL DEFAULT CURRENT_TIMESTAMP,
    create_user        varchar(32)              NOT NULL,
    modify_user        varchar(32)              NOT NULL,
    gmt_modified       timestamp(0)             NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (sysno)
);

ALTER SEQUENCE product_seq RESTART WITH 2;

BEGIN;
INSERT INTO product
VALUES (1, 'cola', 'coco cola', 'cool', '', 'www.baidu.com',
        0, '', 15, 5, 5, 5, 1, '', 1, '2019-05-28 03:36:17', '', '', '2019-06-06 01:37:36');
COMMIT;

DROP TABLE IF EXISTS undo_log;
CREATE SEQUENCE undo_log_seq;
CREATE TABLE undo_log
(
    id            bigint       NOT NULL DEFAULT NEXTVAL('undo_log_seq'),
    branch_id     bigint       NOT NULL,
    xid           varchar(100) NOT NULL,
    context       varchar(128) NOT NULL,
    rollback_info text         NOT NULL,
    log_status    int          NOT NULL,
    log_created   timestamp(0) NOT NULL,
    log_modified  timestamp(0) NOT NULL,
    ext           varchar(100)          DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE
INDEX idx_unionkey ON undo_log (xid,branch_id);
