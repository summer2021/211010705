package dao

import (
	"context"
	"database/sql"
	"fmt"
	"time"
)

import (
	"github.com/google/uuid"
)

const (
	insertSoMaster = `INSERT INTO so_master (
                       sysno, so_id, buyer_user_sysno,
                       seller_company_code, receive_division_sysno,
                       receive_address, receive_zip, receive_contact,
                       receive_contact_phone, stock_sysno, payment_type,
                       so_amt, status, order_date, appid, memo)
                       VALUES (($1),($2),($3),
                               ($4),($5),
                               ($6),($7),($8),
                               ($9),($10),($11),
                               ($12),($13),now(),
                               ($14),($15))`
	insertSoItem = `INSERT INTO so_item(
                    sysno, so_sysno, product_sysno,
                    product_name, cost_price,
                    original_price, deal_price, quantity)
                    VALUES (($1),($2),($3),
                               ($4),($5),
                               ($6),($7),($8))`
)

type Dao struct {
	*sql.DB
}

type SoMaster struct {
	SysNo                int64     `json:"sysNo"`
	SoID                 string    `json:"soID"`
	BuyerUserSysNo       int64     `json:"buyerUserSysNo"`
	SellerCompanyCode    string    `json:"sellerCompanyCode"`
	ReceiveDivisionSysNo int64     `json:"receiveDivisionSysNo"`
	ReceiveAddress       string    `json:"receiveAddress"`
	ReceiveZip           string    `json:"receiveZip"`
	ReceiveContact       string    `json:"receiveContact"`
	ReceiveContactPhone  string    `json:"receiveContactPhone"`
	StockSysNo           int64     `json:"stockSysNo"`
	PaymentType          int32     `json:"paymentType"`
	SoAmt                float64   `json:"soAmt"`
	Status               int32     `json:"status"`
	OrderDate            time.Time `json:"orderDate"`
	PaymentDate          time.Time `json:"paymentDate"`
	DeliveryDate         time.Time `json:"deliveryDate"`
	ReceiveDate          time.Time `json:"receiveDate"`
	AppID                string    `json:"appID"`
	Memo                 string    `json:"memo"`
	CreateUser           string    `json:"createUser"`
	GmtCreate            time.Time `json:"gmtCreate"`
	ModifyUser           string    `json:"modifyUser"`
	GmtModified          time.Time `json:"gmtModified"`

	SoItems []*SoItem
}

type SoItem struct {
	SysNo         int64   `json:"sysNo"`
	SoSysNo       int64   `json:"soSysNo"`
	ProductSysNo  int64   `json:"productSysNo"`
	ProductName   string  `json:"productName"`
	CostPrice     float64 `json:"costPrice"`
	OriginalPrice float64 `json:"originalPrice"`
	DealPrice     float64 `json:"dealPrice"`
	Quantity      int32   `json:"quantity"`
}

func (dao *Dao) CreateSO(ctx context.Context, soMasters []*SoMaster) ([]uint64, error) {
	result := make([]uint64, 0, len(soMasters))
	tx, err := dao.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelDefault,
		ReadOnly:  false,
	})
	if err != nil {
		return nil, err
	}
	for _, soMaster := range soMasters {
		fmt.Println(soMaster)
		soid := NextID()
		_, err = tx.Exec(insertSoMaster, soid, soid, soMaster.BuyerUserSysNo, soMaster.SellerCompanyCode, soMaster.ReceiveDivisionSysNo,
			soMaster.ReceiveAddress, soMaster.ReceiveZip, soMaster.ReceiveContact, soMaster.ReceiveContactPhone, soMaster.StockSysNo,
			soMaster.PaymentType, soMaster.SoAmt, soMaster.Status, soMaster.AppID, soMaster.Memo)
		if err != nil {
			fmt.Println(err)
			tx.Rollback()
			return nil, err
		}
		soItems := soMaster.SoItems
		for _, soItem := range soItems {
			soItemID := NextID()
			_, err = tx.Exec(insertSoItem, soItemID, soid, soItem.ProductSysNo, soItem.ProductName, soItem.CostPrice, soItem.OriginalPrice,
				soItem.DealPrice, soItem.Quantity)
			if err != nil {
				fmt.Println(err)
				tx.Rollback()
				return nil, err
			}
		}
		result = append(result, soid)
	}
	err = tx.Commit()
	if err != nil {
		return nil, err
	}
	return result, nil
}

func NextID() uint64 {
	id, _ := uuid.NewUUID()
	return uint64(id.ID())
}
