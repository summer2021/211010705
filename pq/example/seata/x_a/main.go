package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"time"
)

import (
	"github.com/gin-gonic/gin"
	"github.com/pansicheng/pq"
	"github.com/pansicheng/pq/example/seata/x_a/dao"
	"github.com/transaction-wg/seata-golang/pkg/client"
	"github.com/transaction-wg/seata-golang/pkg/client/config"
)

func main() {
	name := "postgres://postgres:password@localhost:5432/seata_order?sslmode=disable"
	pwd, _ := os.Getwd()
	var configPath = pwd + "/example/seata/x_a/conf/client.yml"
	r := gin.Default()
	config.InitConf(configPath)
	client.NewRpcClient()
	pq.InitDataResourceManager()
	pq.RegisterResource(name)

	connector, err := pq.NewConnector(name)
	if err != nil {
		fmt.Println(err)
		return
	}
	sqlDB := sql.OpenDB(connector)
	defer sqlDB.Close()
	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetMaxIdleConns(20)
	sqlDB.SetConnMaxLifetime(4 * time.Hour)

	d := &dao.Dao{
		DB: sqlDB,
	}

	r.POST("/createSo", func(c *gin.Context) {
		type req struct {
			Req []*dao.SoMaster
		}
		var q req
		if err := c.ShouldBindJSON(&q); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		_, err := d.CreateSO(
			context.WithValue(
				context.Background(),
				pq.XID,
				c.Request.Header.Get("XID")),
			q.Req)

		if err != nil {
			c.JSON(400, gin.H{
				"success": false,
				"message": "fail",
			})
		} else {
			c.JSON(200, gin.H{
				"success": true,
				"message": "success",
			})
		}
	})
	r.Run(":8002")
}
